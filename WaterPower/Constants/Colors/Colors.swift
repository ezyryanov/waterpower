//
//  Colors.swift
//  WaterPower
//
//  Created by Егор Зырянов on 19/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

extension UIImage {
    static func localImage(_ name: String, template: Bool = false) -> UIImage {
        var image = UIImage(named: name)!
        if template {
            image = image.withRenderingMode(.alwaysTemplate)
        }
        return image
    }
}

class Colors: NSObject {
    //static let mainColor = UIColor(red: 103.0/255.0, green: 65.0/255.0, blue: 232.0/255.0, alpha: 1.0)
    static let mainColor = UIColor(red: 25.0/255.0, green: 61.0/255.0, blue: 180.0/255.0, alpha: 1.0)
    
    static let gradientCompanionColor = UIColor(red: 92.0/255.0, green: 81.0/255.0, blue: 212.0/255.0, alpha: 1.0)
    
//    static let containerBackgroundColor1 = UIColor(red: 41.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 1.0)
//
//    static let containerBackgroundColor2 = UIColor(red: 108.0/255.0, green: 101.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    static let containerBackgroundColor2 = UIColor(red: 5.0/255.0, green: 12.0/255.0, blue: 58.0/255.0, alpha: 1.0)
    
    static let containerBackgroundColor1 = UIColor(red: 87.0/255.0, green: 105.0/255.0, blue: 224.0/255.0, alpha: 1.0)
    
    static let progressColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
    //static let progressColor = UIColor(red: 38.0/255.0, green: 208.0/255.0, blue: 29.0/255.0, alpha: 0.5)
    //rgb(38, 208, 29)
}
