//
//  AppDelegate.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit
import CoreData
import Firebase

extension UILabel{
    var defaultFont: UIFont? {
        get { return self.font }
        set { self.font = newValue }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let remoteCongig = RemoteConfig.remoteConfig()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow()
        //AppRouter.shared.goToMain(window: window)
        
        AppRouter.shared.initiateRouter(window: window)
        AppRouter.shared.goToTutorial(window: window)
        FirebaseApp.configure()
        
        configureFirebaseRemoteConfig()
        
        DrinkModel.initiateDrinkModel()
        DrinkCategoryModel.initiateDrinkCategoryModel()
        FoodModel.initiateFoodModel()
        FoodCategoryModel.initiateFoodCategoryModel()
        
        UILabel.appearance().defaultFont = UIFont.systemFont(ofSize: 20)
        
        if let path = Bundle.main.path(forResource: "waterpower_config", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                //let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                CurrentConfig = try! JSONDecoder().decode(Config.self, from: data)
                print("success")
            } catch {
                // handle error
            }
        }
        
        
        return true
    }
    
    func getWindow() -> UIWindow? {
        return window
    }
    
    func configureFirebaseRemoteConfig () {
        //remoteCongig.setDefaults(<#T##defaults: [String : NSObject]?##[String : NSObject]?#>)
        
        remoteCongig.fetch(withExpirationDuration: 10.0, completionHandler: {
            (status,error) in
            self.remoteCongig.activateFetched()
            print(self.remoteCongig.configValue(forKey: "test"))
        })
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        CoreDataManager.shared.saveContext()
    }


}

