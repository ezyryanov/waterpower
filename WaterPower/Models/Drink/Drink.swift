import UIKit

class Drink: Codable {
    var id: Int
    var nameRu: String
    var name: String
    var translates: Translate
    var categoryId: Int
    var hydroCoef: Double
    var isPremium: Bool
    var image: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case nameRu = "name_ru"
        case name
        case translates
        case categoryId = "category_id"
        case hydroCoef = "hydro_coef"
        case isPremium = "is_premium"
        case image = "image"
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! container.decode(Int.self, forKey: .id)
        self.nameRu = try! container.decode(String.self, forKey: .nameRu)
        self.name = try! container.decode(String.self, forKey: .name)
        self.translates = try! container.decode(Translate.self, forKey: .translates)
        self.categoryId = try! container.decode(Int.self, forKey: .categoryId)
        self.hydroCoef = try! container.decode(Double.self, forKey: .hydroCoef)
        self.isPremium = try! container.decode(Bool.self, forKey: .isPremium)
        self.image = try! container.decode(String.self, forKey: .image)
    }
}
