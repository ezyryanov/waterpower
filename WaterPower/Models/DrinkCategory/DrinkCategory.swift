//
//  DrinkCategory.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 11/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class DrinkCategory: Codable {
    var id: Int
    var nameRu: String
    var name: String
    var translates: Translate
    var isSelected: Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case nameRu = "name_ru"
        case name
        case translates
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! container.decode(Int.self, forKey: .id)
        self.nameRu = try! container.decode(String.self, forKey: .nameRu)
        self.name = try! container.decode(String.self, forKey: .name)
        self.translates = try! container.decode(Translate.self, forKey: .translates)
        self.isSelected = false
    }
}
