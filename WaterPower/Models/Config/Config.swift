//
//  Config.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 11/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

var CurrentConfig : Config!

class Config: Codable {
    var drinkCategories: [DrinkCategory]
    var drinks: [Drink]
    var food: [Food]
    var foodCategories: [FoodCategory]
    var volumes: [Volume]
    var weights: [Weight]
    
    enum CodingKeys: String, CodingKey {
        case drinkCategories = "drink_categories"
        case drinks = "drinks"
        case food = "food"
        case foodCategories = "food_categories"
        case volumes = "volumes"
        case weights = "weights"
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.drinkCategories = try! container.decode([DrinkCategory].self, forKey: .drinkCategories)
        self.drinks = try! container.decode([Drink].self, forKey: .drinks)
        self.foodCategories = try! container.decode([FoodCategory].self, forKey: .foodCategories)
        self.food = try! container.decode([Food].self, forKey: .food)
        self.volumes = try! container.decode([Volume].self, forKey: .volumes)
        self.weights = try! container.decode([Weight].self, forKey: .weights)
        print("success")
    }
}
