//
//  Volume.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 11/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class Volume: Codable {
    var id: Int
    var mlValue: Double
    var ozValue: Double
    var isMlShowing : Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case mlValue = "ml_value"
        case ozValue = "oz_value"
        case isMlShowing = "is_ml_showing"
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! container.decode(Int.self, forKey: .id)
        self.mlValue = try! container.decode(Double.self, forKey: .mlValue)
        self.ozValue = try! container.decode(Double.self, forKey: .ozValue)
        self.isMlShowing = try! container.decode(Bool.self, forKey: .isMlShowing)
    }
}
