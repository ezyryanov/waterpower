import UIKit

class Translate: Codable {
    var en : String!
    var de : String!
    var fr : String!
    var it : String!
    var es : String!
    var zh : String!
    var pt : String!
    var ja : String!
    var ko : String!
    var ru : String!
    
    enum CodingKeys: String, CodingKey {
        case en
        case de
        case fr
        case it
        case es
        case zh
        case pt
        case ja
        case ko
        case ru
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.en = try? container.decode(String.self, forKey: .en)
        self.de = try? container.decode(String.self, forKey: .de)
        self.fr = try? container.decode(String.self, forKey: .fr)
        self.it = try? container.decode(String.self, forKey: .it)
        self.es = try? container.decode(String.self, forKey: .es)
        self.zh = try? container.decode(String.self, forKey: .zh)
        self.pt = try? container.decode(String.self, forKey: .pt)
        self.ja = try? container.decode(String.self, forKey: .ja)
        self.ko = try? container.decode(String.self, forKey: .ko)
        self.ru = try? container.decode(String.self, forKey: .ru)
    }
}
