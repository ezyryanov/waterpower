//
//  FoodCategoryModel.swift
//  WaterPower
//
//  Created by Егор Зырянов on 09/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class FoodCategoryModel: NSObject {
    static var foodCategoryArray : [FoodCategoryModel] = Array()
    
    var name : String = "NONE"
    var id : NSInteger = 0
    var isSelected : Bool = false
    
    init(id : NSInteger, name : String) {
        self.id = id
        self.name = name
    }
    
    static func getCategoryById(id : NSInteger) -> FoodCategoryModel {
        for foodCategoryModel in foodCategoryArray {
            if foodCategoryModel.id == id {
                return foodCategoryModel
            }
        }
        return foodCategoryArray.first!
    }
    
    static func initiateFoodCategoryModel() {
        foodCategoryArray.append(FoodCategoryModel(id: 0, name: "ALL"))
        foodCategoryArray[0].isSelected = true
        foodCategoryArray.append(FoodCategoryModel(id: 1, name: "FOOD-CATEGORY1"))
        foodCategoryArray.append(FoodCategoryModel(id: 2, name: "FOOD-CATEGORY2"))
        foodCategoryArray.append(FoodCategoryModel(id: 3, name: "FOOD-CATEGORY3"))
        foodCategoryArray.append(FoodCategoryModel(id: 4, name: "FOOD-CATEGORY4"))
    }
}
