import UIKit

class ObjectModel: NSObject {

    var isVolumeObject = true
    
    var objectName : String = "unknown"
    
    var volumeEU : CGFloat = 0
    var nameVolumeEU : String = "ml"
    var volumeUS : CGFloat = 0
    var nameVolumeUS : String = "oz"
    
    var weightEU : CGFloat = 0
    var nameWeightEU : String = "gr"
    var weightUS : CGFloat = 0
    var nameWeightUS : String = "lb"
    
    init(objectName : String, volumeEU : CGFloat, volumeUS : CGFloat) {
        self.objectName = objectName
        self.volumeEU = volumeEU
        self.volumeUS = volumeUS
        self.isVolumeObject = true
    }
    
    init(objectName : String,weightEU : CGFloat, weightUS : CGFloat) {
        self.objectName = objectName
        self.weightEU = weightEU
        self.weightUS = weightUS
        self.isVolumeObject = false
    }
}
