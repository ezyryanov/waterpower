//
//  DrinkCategoryModel.swift
//  WaterPower
//
//  Created by Егор Зырянов on 09/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class DrinkCategoryModel: NSObject {
    static var drinkCategoryArray : [DrinkCategoryModel] = Array()
    
    var name : String = "NONE"
    var id : NSInteger = 0
    var isSelected : Bool = false
    
    init(id : NSInteger, name : String) {
        self.id = id
        self.name = name
    }
    
    static func getCategoryById(id : NSInteger) -> DrinkCategoryModel {
        for drinkCategoryModel in drinkCategoryArray {
            if drinkCategoryModel.id == id {
                return drinkCategoryModel
            }
        }
        return drinkCategoryArray.first!
    }
    
    static func initiateDrinkCategoryModel() {
        drinkCategoryArray.append(DrinkCategoryModel(id: 0, name: "ALL"))
        drinkCategoryArray[0].isSelected = true
        drinkCategoryArray.append(DrinkCategoryModel(id: 1, name: "CATEGORY1"))
        drinkCategoryArray.append(DrinkCategoryModel(id: 2, name: "CATEGORY2"))
        drinkCategoryArray.append(DrinkCategoryModel(id: 3, name: "CATEGORY3"))
        drinkCategoryArray.append(DrinkCategoryModel(id: 4, name: "CATEGORY4"))
    }
}
