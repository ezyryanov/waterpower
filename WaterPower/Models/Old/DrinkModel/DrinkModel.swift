//
//  DrinkModel.swift
//  WaterPower
//
//  Created by Егор Зырянов on 07/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class DrinkModel: NSObject {
    static var drinkArray : [DrinkModel] = Array()
    
    var name : String = "NONE"
    var id : NSInteger = 0
    var imageName : String = "DefaultDrink"
    var coef : CGFloat = 1.0
    var weightsArray = [WeightModel]()
    var objectsArray = [ObjectModel]()
    
    init(id : NSInteger, name : String, imageName : String, coef : CGFloat, categoryId: NSInteger) {
        self.id = id
        self.name = name
        self.imageName = imageName
    }
    
    static func getDrinkById(id : NSInteger) -> DrinkModel {
        for drinkModel in drinkArray {
            if drinkModel.id == id {
                return drinkModel
            }
        }
        return drinkArray.first!
    }
    
    static func initiateDrinkModel() {
        drinkArray.append(DrinkModel(id: 11, name: "WATER", imageName: "water", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 12, name: "TEA", imageName: "coffee-cup", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 13, name: "COFFEE", imageName: "coffee", coef : 0.90, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 14, name: "MINERAL_WATER", imageName: "water-bottle", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 15, name: "COLA", imageName: "mineral_water", coef : 0.89, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 16, name: "DIET_COLA", imageName: "mineral_water", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 17, name: "MILK", imageName: "water", coef : 1.30, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 18, name: "JUICE", imageName: "fruit", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))
        
        
        
        drinkArray.append(DrinkModel(id: 19, name: "BEER", imageName: "beer", coef : 1.00, categoryId: 1))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 100.0, volumeUS: 3.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 200.0, volumeUS: 10))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 250.0, volumeUS: 13.5))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 300.0, volumeUS: 17.0))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 350.0, volumeUS: 20))
        drinkArray.last!.weightsArray.append(WeightModel(volumeEU: 400.0, volumeUS: 23.5))
        
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Cup", volumeEU: 150.0, volumeUS: 7.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Glass", volumeEU: 200.0, volumeUS: 10.0))
        drinkArray.last!.objectsArray.append(ObjectModel(objectName: "Big cup", volumeEU: 300.0, volumeUS: 13.5))

    }
}
