//
//  WeightModel.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class WeightModel: NSObject {

    var isVolume = true
    
    var volumeEU : CGFloat = 0
    var nameVolumeEU : String = "ml"
    var volumeUS : CGFloat = 0
    var nameVolumeUS : String = "oz"
    
    var weightEU : CGFloat = 0
    var nameWeightEU : String = "gr"
    var weightUS : CGFloat = 0
    var nameWeightUS : String = "lb"
    
    init(volumeEU : CGFloat, volumeUS : CGFloat) {
        self.volumeEU = volumeEU
        self.volumeUS = volumeUS
        self.isVolume = true
    }
    
    init(weightEU : CGFloat, weightUS : CGFloat) {
        self.weightEU = weightEU
        self.weightUS = weightUS
        self.isVolume = false
    }
}
