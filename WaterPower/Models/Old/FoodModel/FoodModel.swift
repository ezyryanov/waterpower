//
//  FoodModel.swift
//  WaterPower
//
//  Created by Егор Зырянов on 09/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class FoodModel: NSObject {
    static var foodArray : [FoodModel] = Array()
    
    var name : String = "NONE"
    var id : NSInteger = 0
    var imageName : String = "DefaultFood"
    var coef : CGFloat = 1.0
    var weightsArray = [WeightModel]()
    var objectsArray = [ObjectModel]()
    
    init(id : NSInteger, name : String, imageName : String, coef : CGFloat, categoryId: NSInteger) {
        self.id = id
        self.name = name
        self.imageName = imageName
    }
    
    static func getFoodById(id : NSInteger) -> FoodModel {
        for foodModel in foodArray {
            if foodModel.id == id {
                return foodModel
            }
        }
        return foodArray.first!
    }
    
    static func initiateFoodModel() {
        foodArray.append(FoodModel(id: 1, name: "FOOD1", imageName: "home", coef : 0.95, categoryId: 1))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        
        foodArray.append(FoodModel(id: 2, name: "FOOD2", imageName: "home", coef : 0.95, categoryId: 2))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        foodArray.append(FoodModel(id: 3, name: "FOOD3", imageName: "home", coef : 0.95, categoryId: 3))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        
        foodArray.append(FoodModel(id: 4, name: "FOOD4", imageName: "home", coef : 0.95, categoryId: 4))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        
        foodArray.append(FoodModel(id: 5, name: "FOOD5", imageName: "home", coef : 0.95, categoryId: 1))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        foodArray.append(FoodModel(id: 6, name: "FOOD6", imageName: "home", coef : 0.95, categoryId: 2))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        
        
        foodArray.append(FoodModel(id: 7, name: "FOOD7", imageName: "home", coef : 0.95, categoryId: 3))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
        
        
        foodArray.append(FoodModel(id: 8, name: "FOOD8", imageName: "home", coef : 0.95, categoryId: 4))
        foodArray.last!.objectsArray.append(ObjectModel(objectName: "pieces", weightEU: 200.0, weightUS: 8.0))
        
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 50.0, weightUS: 2.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 100.0, weightUS: 4.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 150.0, weightUS: 6.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 200.0, weightUS: 8.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 250.0, weightUS: 10.0))
        foodArray.last!.weightsArray.append(WeightModel(weightEU: 300.0, weightUS: 12.0))
        
    }
}
