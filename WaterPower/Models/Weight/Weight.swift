//
//  Weight.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 11/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class Weight: Codable {
    var id: Int
    var grValue: Double
    var lbValue: Double
    var isGrShowing : Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case grValue = "gr_value"
        case lbValue = "lb_value"
        case isGrShowing = "is_gr_showing"
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try! container.decode(Int.self, forKey: .id)
        self.grValue = try! container.decode(Double.self, forKey: .grValue)
        self.lbValue = try! container.decode(Double.self, forKey: .lbValue)
        self.isGrShowing = try! container.decode(Bool.self, forKey: .isGrShowing)
    }
}
