//
//  ThirdPageTourViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class ThirdPageTourViewController: TourBasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("FIRSTTOUR", comment: "")
        
        let tourImage = UIImage.localImage("reward_tour", template: true)
        tourImageView.image = tourImage
        advantageText.text = "Мотивируй себя, используя встроенные в приложение достижения!"
        tourImageView.tintColor = Colors.mainColor
        // Do any additional setup after loading the view.
    }
    

    override func continueButtonClicked() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.getWindow()
        //self.navigationController?.popToRootViewController(animated: false)
        self.show(SubscriptionViewController(), sender: nil)
    }

}
