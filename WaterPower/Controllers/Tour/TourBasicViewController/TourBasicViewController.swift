//
//  TourBasicViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class TourBasicViewController: UIViewController {
    
    let fontView : UIImageView = UIImageView()
    let containerView : UIView = UIView()
    let containerViewShadow : UIView = UIView()
    let continueButton = UIButton()
    let advantageText = UILabel()
    let tourImageView = UIImageView()
    let inset : CGFloat = 10.0
    let buttonHeight : CGFloat = 56.0

    override func viewDidLoad() {
        super.viewDidLoad()

//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.view.bounds
//        gradientLayer.colors = [Colors.containerBackgroundColor1.cgColor, Colors.containerBackgroundColor2.cgColor]
        view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        //self.view.layer.addSublayer(gradientLayer)
        
        // Do any additional setup after loading the view.
        createElements()
    }
    
    func createElements() {
        self.view.addSubview(containerViewShadow)
        containerViewShadow.addSubview(containerView)
        containerView.addSubview(continueButton)
        containerView.addSubview(advantageText)
        containerView.addSubview(tourImageView)
        
        containerView.layer.shadowOffset = CGSize(width: 0, height: 10.0)
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowRadius = 10.0
        containerView.layer.shadowOpacity = 0.5
        
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 25.0
        
        let verticalCoef : CGFloat = 0.1
        let horizontalCoef : CGFloat = 0.1
        containerViewShadow.frame = CGRect(x: self.view.frame.width * horizontalCoef, y: self.view.frame.height * verticalCoef, width: self.view.frame.width * (1.0 - 2 * horizontalCoef), height: self.view.frame.height * (1.0 - 2 * verticalCoef))
        
        containerView.frame = CGRect(x: 0.0, y: 0.0, width: containerViewShadow.frame.width, height: containerViewShadow.frame.height)
        
        var currentPosition : CGFloat = inset
        advantageText.frame = CGRect(x: inset, y: currentPosition, width: containerView.frame.width - 2.0 * inset, height: containerView.frame.height * 0.3)
        currentPosition += inset + containerView.frame.height * 0.3
        advantageText.numberOfLines = 0
        advantageText.lineBreakMode = .byWordWrapping
        advantageText.textColor = Colors.mainColor
        advantageText.textAlignment = .center
        
        tourImageView.frame = CGRect(x: 4.0 * inset, y: advantageText.frame.maxY + inset, width: containerView.frame.width - 8.0 * inset, height: containerView.frame.height - buttonHeight - containerView.frame.height * 0.3 - 8.0 * inset)
        
        
        continueButton.frame = CGRect(x: inset, y: containerView.frame.height - buttonHeight - inset, width: containerView.frame.width - 2.0 * inset, height: buttonHeight)
        continueButton.layer.cornerRadius = buttonHeight/2.0
        continueButton.backgroundColor = Colors.mainColor
        continueButton.setTitle("CONTINUE", for: .normal)
        continueButton.addTarget(self, action: #selector(continueButtonClicked), for: UIControl.Event.touchDown)
        continueButton.isUserInteractionEnabled = true
        

    }
    
    @objc func continueButtonClicked() {
        
    }

    

}
