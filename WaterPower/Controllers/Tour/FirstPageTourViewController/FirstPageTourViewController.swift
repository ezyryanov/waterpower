//
//  FirstPageTourViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class FirstPageTourViewController: TourBasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("THIRDTOUR", comment: "")
        
        let tourImage = UIImage.localImage("glass_tour", template: true)
        advantageText.text = "Добавляй напитки и еду, чтобы контролировать уровень жидкости в организме!"
        tourImageView.image = tourImage
        tourImageView.tintColor = Colors.mainColor
    }
    

    override func continueButtonClicked() {
        let vc = SecondPageTourViewController()
        self.show(vc, sender: self)
    }

}
