//
//  SecondPageTourViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class SecondPageTourViewController: TourBasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        title = NSLocalizedString("SECONDTOUR", comment: "")
        // Do any additional setup after loading the view.
        
        let tourImage = UIImage.localImage("analytics_tour", template: true)
        advantageText.text = "Следи за своим прогрессом с помощью аналитики!"
        tourImageView.image = tourImage
        tourImageView.tintColor = Colors.mainColor
    }
    

    override func continueButtonClicked() {
        let vc = ThirdPageTourViewController()
        self.show(vc, sender: self)
    }

}
