//
//  NewsViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class NewsViewController: BasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.navigationItem.title = NSLocalizedString("ACHIEVMENTS", comment: "")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
