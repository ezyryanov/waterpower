//
//  BasicViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 11/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class BasicViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        // Do any additional setup after loading the view
        
        navigationController?.navigationBar.tintColor = Colors.mainColor
        
        
    }
    
    @objc func showSettings() {
        let vc = SettingsViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
