//
//  TabbarViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        UITabBar.appearance().tintColor = Colors.mainColor
        self.tabBar.barTintColor = .white
        
//        let newsViewController = NewsViewController()
//        let newsIcon = UITabBarItem(title: "Достижения", image: UIImage(named: "news.pdf"), selectedImage: UIImage(named: "news.pdf"))
//        newsViewController.tabBarItem = newsIcon
        
        let alarmViewController = AlarmViewController()
        let alarmIcon = UITabBarItem(title: "Напоминания", image: UIImage(named: "alarm.pdf"), selectedImage: UIImage(named: "alarm.pdf"))
        alarmViewController.tabBarItem = alarmIcon
        
        let analyticsViewController = AnalyticsViewController()
        let analyticsIcon = UITabBarItem(title: "Статистика", image: UIImage(named: "analytics.pdf"), selectedImage: UIImage(named: "analytics.pdf"))
        analyticsViewController.tabBarItem = analyticsIcon
        
        let homeViewController = HomeViewController()
        let homeIcon = UITabBarItem(title: "Главная", image: UIImage(named: "home.pdf"), selectedImage: UIImage(named: "home.pdf"))
        homeViewController.tabBarItem = homeIcon
        
        
        let controllers = [homeViewController, analyticsViewController, alarmViewController]  //array of the root view controllers displayed by the tab bar interface
        self.viewControllers = controllers
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        super.viewWillAppear(animated)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
