//
//  SettingsViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class SettingsViewController: BasicViewController, UITableViewDelegate, UITableViewDataSource {
    
    let settingsTable = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTable()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.items?.first?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setUpTable() {
        settingsTable.frame = view.frame
        view.addSubview(settingsTable)
        settingsTable.backgroundColor = .clear
        settingsTable.dataSource = self
        settingsTable.delegate = self
        settingsTable.separatorColor = Colors.mainColor
        settingsTable.bounces = false
        settingsTable.bouncesZoom = false
        settingsTable.separatorInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        settingsTable.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 4;
        case 1:
            return 3;
        default:
            return 0;
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.textColor = Colors.mainColor
        switch indexPath.section {
        case 0:
            switch indexPath.row {
                case 0:
                    cell.textLabel?.text = "Ежедневная норма воды"
                    break
                case 1:
                    cell.textLabel?.text = "Еденицы измерения"
                    break
                case 2:
                    cell.textLabel?.text = "Информация о напитках"
                    break
                case 3:
                    cell.textLabel?.text = "Информация о еде"
                    break
                default:
                    break
            }
            break
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Оценить приложение"
                break
            case 1:
                cell.textLabel?.text = "Написать в службу поддержки"
                break
            case 2:
                cell.textLabel?.text = "Получить доступ к премиум версии"
                break
            default:
                break
            }
        default:
            break
        }
        
        return cell;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.navigationItem.title = NSLocalizedString("SETTINGS", comment: "")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
