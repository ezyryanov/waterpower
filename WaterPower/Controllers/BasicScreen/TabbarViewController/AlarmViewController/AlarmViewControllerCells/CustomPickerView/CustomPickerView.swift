//
//  CustomPickerView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 26/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit
import CoreData

class CustomPickerView: UIPickerView {

    public var hours : String = "06"
    public var minutes : String = "10"
    public let firstComponentArray = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
    
    public let secondComponentArray = ["00","01","05","10","15","20","25","30","35","40","45","50","55"]
    public var type : String = "unknown"
    
    
    
    override func select(_ sender: Any?) {
        super.select(sender)
        
//        var periods = [Periods]()
//        
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Periods")
//        
//        do {
//            periods = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest) as! [Periods]
//        } catch {
//            print(error.localizedDescription)
//        }
//        
//        for period in periods {
//            let periodItem = period as Periods
//            if (periodItem.identifier == type) {
//                periodItem.hours = firstComponentArray[self.selectedRow(inComponent: 0)]
//                periodItem.minutes = secondComponentArray[self.selectedRow(inComponent: 1)]
//                hours = firstComponentArray[self.selectedRow(inComponent: 0)]
//                minutes = secondComponentArray[self.selectedRow(inComponent: 1)]
//            }
//        }
//        
//        CoreDataManager.shared.saveContext()
    }
    
    override func selectRow(_ row: Int, inComponent component: Int, animated: Bool) {
        super.selectRow(row, inComponent: component, animated: animated)
        
        
    }

}
