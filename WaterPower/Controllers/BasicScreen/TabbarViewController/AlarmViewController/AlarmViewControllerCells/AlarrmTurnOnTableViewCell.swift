//
//  AlarrmTurnOnTableViewCell.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 24/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit
import UserNotifications

protocol AlarrmTurnOnTableViewCellDelegate {
    func enableNotification()
}

class AlarrmTurnOnTableViewCell: UITableViewCell {
    
    let turnOnSwitch = UISwitch(frame: CGRect.zero)
    let infoLabel = UILabel()
    let inset : CGFloat = 10.0
    let switchWidth : CGFloat = 60.0
    var tableView : UITableView!
    var delegate : AlarrmTurnOnTableViewCellDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        
        infoLabel.textColor = Colors.mainColor
        infoLabel.font = UIFont(name: infoLabel.font.fontName, size: 20.0)
        infoLabel.text = "Включить уведомления"
        
        turnOnSwitch.tintColor = Colors.mainColor
        turnOnSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        
        self.addSubview(infoLabel)
        self.addSubview(turnOnSwitch)
    }
    
    @objc func switchChanged() {
        if (turnOnSwitch.isOn) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                if (granted) {
                    let defaults = UserDefaults.standard
                    defaults.set(true, forKey: "isPushOn")
                    defaults.synchronize()
                } else {
                    let defaults = UserDefaults.standard
                    defaults.set(false, forKey: "isPushOn")
                    defaults.synchronize()
                }
            }
        } else {
            let defaults = UserDefaults.standard
            defaults.set(false, forKey: "isPushOn")
            defaults.synchronize()
        }
        delegate.enableNotification()
    }
    
    override var frame: CGRect {
        didSet {
            infoLabel.sizeToFit()
            if (infoLabel.frame.width > (self.frame.width - 3 * inset - switchWidth)) {
                infoLabel.frame = CGRect(x: inset, y: (self.frame.height - 2.0  * infoLabel.frame.height)/2.0, width: infoLabel.frame.width/2.0, height: infoLabel.frame.height * 2.0)
            } else {
                infoLabel.frame = CGRect(x: inset, y: (self.frame.height - infoLabel.frame.height)/2.0, width: infoLabel.frame.width, height: infoLabel.frame.height)
            }
            
            turnOnSwitch.frame = CGRect(x: self.frame.width - turnOnSwitch.frame.width - inset, y: (self.frame.height - turnOnSwitch.frame.height)/2.0, width: turnOnSwitch.frame.width, height: turnOnSwitch.frame.height)
            
            let defaults = UserDefaults.standard
            if (defaults.bool(forKey: "isPushOn")) {
                turnOnSwitch.setOn(true, animated: true)
            } else {
                turnOnSwitch.setOn(false, animated: true)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
