

import UIKit
import CoreData


class AlarmPeriodTableViewCell: UITableViewCell, UIPickerViewDelegate,UIPickerViewDataSource {
    
    let pickerWidth : CGFloat = 120.0
    let pickerView = CustomPickerView(frame: CGRect(x: 0.0, y: 0.0, width: 120.0, height: 50.0))
    let infoLabel = UILabel()
    let inset : CGFloat = 10.0
    let disableView : UIView = UIView()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, text : String, pickerType: String) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        
        infoLabel.textColor = Colors.mainColor
        infoLabel.font = UIFont(name: infoLabel.font.fontName, size: 20.0)
        infoLabel.text = text
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        disableView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.addSubview(infoLabel)
        self.addSubview(pickerView)
        self.addSubview(disableView)
        
        pickerView.type = pickerType
    
        var periods = [Periods]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Periods")
        
        do {
            periods = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest) as! [Periods]
        } catch {
            print(error.localizedDescription)
        }
        var isFound = false
        for period in periods {
            let periodItem = period as Periods
            if (periodItem.identifier == pickerType) {
                pickerView.hours = periodItem.hours!
                pickerView.minutes = periodItem.minutes!
                isFound = true
            }
        }
        
        if !isFound {
            let entity =  NSEntityDescription.entity(forEntityName: "Periods", in: CoreDataManager.shared.persistentContainer.viewContext)
            let analytics = NSManagedObject(entity: entity!, insertInto: CoreDataManager.shared.persistentContainer.viewContext)
            
            analytics.setValue(pickerType, forKey: "identifier")
            analytics.setValue(pickerView.hours, forKey: "hours")
            analytics.setValue(pickerView.minutes, forKey: "minutes")
            
            CoreDataManager.shared.saveContext()
        }
        
        for hoursString in pickerView.firstComponentArray {
            if (hoursString == self.pickerView.hours) {
                pickerView.selectRow(self.pickerView.firstComponentArray.firstIndex(of: self.pickerView.hours)!, inComponent: 0, animated: false)
            }
        }
        
        for minutesString in pickerView.secondComponentArray {
            if (minutesString == self.pickerView.minutes) {
                pickerView.selectRow(self.pickerView.secondComponentArray.firstIndex(of: self.pickerView.minutes)!, inComponent: 1, animated: false)
            }
        }
    }
    
    override var frame: CGRect {
        didSet {
            infoLabel.sizeToFit()
            if (infoLabel.frame.width > (self.frame.width - 3 * inset - pickerWidth)) {
                infoLabel.frame = CGRect(x: inset, y: (self.frame.height - 2.0  * infoLabel.frame.height)/2.0, width: infoLabel.frame.width/2.0, height: infoLabel.frame.height * 2.0)
            } else {
                infoLabel.frame = CGRect(x: inset, y: (self.frame.height - infoLabel.frame.height)/2.0, width: infoLabel.frame.width, height: infoLabel.frame.height)
            }
            
            pickerView.frame = CGRect(x: self.frame.width - pickerView.frame.width - inset, y: (self.frame.height - pickerView.frame.height)/2.0, width: pickerView.frame.width, height: pickerView.frame.height)
            
            let defaults = UserDefaults.standard
            if (!defaults.bool(forKey: "isPushOn")) {
                disableView.frame = self.bounds
            } else {
                disableView.frame = .zero
            }
        }
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return self.pickerView.firstComponentArray.count
        case 1:
            return self.pickerView.secondComponentArray.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var periods = [Periods]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Periods")
        
        do {
            periods = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest) as! [Periods]
        } catch {
            print(error.localizedDescription)
        }
        
        for period in periods {
            let periodItem = period as Periods
            if (periodItem.identifier == self.pickerView.type) {
                periodItem.hours = self.pickerView.firstComponentArray[self.pickerView.selectedRow(inComponent: 0)]
                periodItem.minutes = self.pickerView.secondComponentArray[self.pickerView.selectedRow(inComponent: 1)]
                self.pickerView.hours = self.pickerView.firstComponentArray[self.pickerView.selectedRow(inComponent: 0)]
                self.pickerView.minutes = self.pickerView.secondComponentArray[self.pickerView.selectedRow(inComponent: 1)]
            }
        }
        
        NotificationManager.shared.createNotifications()
        
        CoreDataManager.shared.saveContext()
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return self.pickerView.firstComponentArray[row]
        case 1:
            return self.pickerView.secondComponentArray[row]
        default:
            return ""
        }
    }

}
