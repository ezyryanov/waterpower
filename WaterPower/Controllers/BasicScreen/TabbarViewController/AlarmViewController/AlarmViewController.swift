//
//  AlarmViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class AlarmViewController: BasicViewController,UITableViewDataSource,UITableViewDelegate,AlarrmTurnOnTableViewCellDelegate {
    

    let alarmTable = UITableView()
    let cellHeight : CGFloat = 56.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTable()
        
        let settingBarItem = UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(showSettings))
        navigationController?.navigationBar.items?.first?.rightBarButtonItem = settingBarItem
    }
    
    func setUpTable() {
        alarmTable.frame = view.frame
        view.addSubview(alarmTable)
        alarmTable.backgroundColor = .clear
        alarmTable.dataSource = self
        alarmTable.delegate = self
        alarmTable.separatorColor = Colors.mainColor
        alarmTable.bounces = false
        alarmTable.bouncesZoom = false
        alarmTable.tableFooterView = UIView()
        alarmTable.separatorInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1;
        case 1:
            return 3;
        default:
            return 0;
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0 && indexPath.row == 0) {
            let cell = AlarrmTurnOnTableViewCell(style: .default, reuseIdentifier: "turnon")
            cell.tableView = tableView
            cell.delegate = self
            return cell
        } else {
            let cell : AlarmPeriodTableViewCell!
            switch indexPath.row {
            case 0:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Начало дня",pickerType: "start_day")
                break
            case 1:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Конец дня",pickerType:"end_day")
                break
            case 2:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Интервал",pickerType:"interval")
                break
            default:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Неизвестно",pickerType:"interval")
                break
            }
            return cell
        }
    }
    
    func enableNotification() {
        alarmTable.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.navigationItem.title = NSLocalizedString("ALARM", comment: "")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
