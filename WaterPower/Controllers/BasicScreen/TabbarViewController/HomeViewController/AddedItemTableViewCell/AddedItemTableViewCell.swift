import UIKit

class AddedItemTableViewCell: UITableViewCell {

    let itemImageView = UIImageView()
    let itemNameLabel = UILabel()
    let weightLabel = UILabel()
    let dateLabel = UILabel()
    
    let inset : CGFloat = 10.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, item : Analytics) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(itemImageView)
        self.addSubview(itemNameLabel)
        self.itemNameLabel.textColor = Colors.mainColor
        self.addSubview(weightLabel)
        self.weightLabel.textColor = Colors.mainColor
        self.addSubview(dateLabel)
        self.dateLabel.textColor = Colors.mainColor
        if (item.isDrink) {
            let drinkModel = DrinkModel.getDrinkById(id: NSInteger(item.drinkID))
            let itemImage = UIImage.localImage(drinkModel.imageName, template: true)
            self.itemImageView.image = itemImage
            self.itemImageView.tintColor = Colors.mainColor
            
            self.itemNameLabel.text = drinkModel.name
            
            self.weightLabel.text = "200 ml"
            
            self.tag = drinkModel.id
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm"
            
            let currentDateString = formatter.string(from: item.date! as Date)
            self.dateLabel.text = currentDateString
            
        }
    }
    
    override var frame: CGRect {
        didSet {
            
            itemImageView.frame = CGRect(x: inset, y: inset, width: self.frame.height - 2.0 * inset, height: self.frame.height - 2.0 * inset)
            
            itemNameLabel.frame = CGRect(x: itemImageView.frame.maxX + inset, y: inset, width: (self.frame.width - itemImageView.frame.maxX) * 0.7, height: itemImageView.frame.height/2.0)
            
            weightLabel.frame = CGRect(x: itemImageView.frame.maxX + inset, y: itemNameLabel.frame.maxY, width: (self.frame.width - itemImageView.frame.maxX) * 0.7, height: itemImageView.frame.height/2.0)
            
            dateLabel.frame = CGRect(x: itemNameLabel.frame.maxX, y: inset + itemImageView.frame.height/4.0, width: (self.frame.width - itemImageView.frame.maxX) * 0.25, height: itemImageView.frame.height/2.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
