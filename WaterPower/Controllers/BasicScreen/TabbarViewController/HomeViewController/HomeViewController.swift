//
//  HomeViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: BasicViewController {

    let dateSpinner = DateSpinner(frame: CGRect.zero)
    let progressView = ProgressView(frame: CGRect.zero)
    let itemsTableView = ItemsTableView(frame: CGRect.zero)
    let addDrinkButton = AddItemButton(frame: CGRect.zero)
    //let addFoodButton = AddItemButton(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateSpinner.itemsTableView = itemsTableView
        createElements()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        placeElements()
        
        let defaults = UserDefaults.standard
        let isFirstLaunch = defaults.bool(forKey: "isFirstLaunch")
        if (!isFirstLaunch) {
            var initialSettingScreen = InitialSplashScreen(frame: .zero)
            initialSettingScreen.frame = self.view.bounds
            self.view.addSubview(initialSettingScreen)
        }
        
        let settingBarItem = UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(showSettings))
        navigationController?.navigationBar.items?.first?.rightBarButtonItem = settingBarItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        placeElements()
        self.tabBarController?.navigationItem.title = NSLocalizedString("HOME", comment: "")
        
        var analytics = [Analytics]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Analytics")
        
        do {
            analytics = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest) as! [Analytics]
        } catch {
            print(error.localizedDescription)
        }
        for analytic in analytics {
            print(analytic.drinkID)
        }
        itemsTableView.setItemsArray(itemsArray: analytics)
        itemsTableView.tableView.reloadData()
        
    }
    
    func createElements() {
        dateSpinner.frame = CGRect(x: 0, y: 0.0, width: view.frame.width, height: 48.0)
        //dateSpinner.backgroundColor = .yellow
        view.addSubview(dateSpinner)
        
        progressView.frame = CGRect(x: 0, y: dateSpinner.frame.maxY, width: view.frame.width, height: 56.0)
        //progressView.backgroundColor = .green
        view.addSubview(progressView)
        
        let minY = (self.tabBarController?.tabBar.frame.minY)!
        addDrinkButton.frame = CGRect(x: 0, y: minY - 56.0, width: view.frame.width, height: 56.0)
        //addDrinkButton.backgroundColor = .green
        addDrinkButton.setTitle(titleString: "Добавить напиток".uppercased())
        view.addSubview(addDrinkButton)
        addDrinkButton.addTargetToButton(target: self, action: #selector(addDrinkButtonWasClicked))
        
//        addFoodButton.frame = CGRect(x: 0, y: minY -  56.0, width: view.frame.width, height: 56.0)
//        //addFoodButton.backgroundColor = .yellow
//        addFoodButton.setTitle(titleString: "Добавить еду".uppercased())
//        view.addSubview(addFoodButton)
        
        itemsTableView.frame = CGRect(x: 0, y: progressView.frame.maxY, width: view.frame.width, height: addDrinkButton.frame.minY - progressView.frame.maxY)
        //itemsTableView.backgroundColor = .red
        view.addSubview(itemsTableView)
        //addFoodButton.addTargetToButton(target: self, action: #selector(addFoodButtonWasClicked))
    }
    
    func placeElements() {
        dateSpinner.frame = CGRect(x: 0, y: 0.0, width: view.frame.width, height: 56.0)
        progressView.frame = CGRect(x: 0, y: dateSpinner.frame.maxY, width: view.frame.width, height: 56.0)
        let minY = (self.tabBarController?.tabBar.frame.minY)!
        addDrinkButton.frame = CGRect(x: 0, y: minY - 56.0, width: view.frame.width, height: 56.0)
        //addFoodButton.frame = CGRect(x: 0, y: minY -  56.0, width: view.frame.width, height: 56.0)
        itemsTableView.frame = CGRect(x: 0, y: progressView.frame.maxY, width: view.frame.width, height: addDrinkButton.frame.minY - progressView.frame.maxY)
    }
    
    
    @objc func addDrinkButtonWasClicked() {
        let addItemViewController = AddItemViewController()
        addItemViewController.isDrinkController = true
        self.navigationController?.pushViewController(addItemViewController, animated: true)
    }
    
    @objc func addFoodButtonWasClicked() {
        let addItemViewController = AddItemViewController()
        addItemViewController.isDrinkController = false
        self.navigationController?.pushViewController(addItemViewController, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
