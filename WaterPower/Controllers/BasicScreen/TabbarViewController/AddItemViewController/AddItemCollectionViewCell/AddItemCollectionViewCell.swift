//
//  AddItemCollectionViewCell.swift
//  WaterPower
//
//  Created by Егор Зырянов on 08/06/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class AddItemCollectionViewCell: UICollectionViewCell {
    
    var imageView = UIImageView()
    var nameLabel = UILabel()
    let inset : CGFloat = 10.0
    let textHeight : CGFloat = 30.0
    var itemId = 0
    
    override init(frame : CGRect) {
        super.init(frame: frame)
        self.addSubview(imageView)
        self.addSubview(nameLabel)
        
        nameLabel.textAlignment = .center
        nameLabel.textColor = Colors.mainColor
        nameLabel.font = nameLabel.font.withSize(20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func putModel(drink : Drink) {
        let drinkImage = UIImage.localImage(drink.image, template: true)
        self.imageView.image = drinkImage
        self.imageView.tintColor = Colors.mainColor
        
        self.nameLabel.text = drink.name
        
        self.itemId = drink.id
        print(String(drink.id))
    }
    
    
    func putModel(food : Food) {
        let drinkImage = UIImage.localImage(food.image, template: true)
        self.imageView.image = drinkImage
        self.imageView.tintColor = Colors.mainColor
        
        self.nameLabel.text = food.name
        
        self.itemId = food.id
    }
    
    override var frame: CGRect {
        didSet {
            let imageHeight : CGFloat = self.frame.height - 3 * inset - textHeight
            imageView.frame = CGRect(x: (self.frame.width - imageHeight)/2.0, y: inset, width: imageHeight, height: imageHeight)
            
            nameLabel.frame = CGRect(x: inset, y: imageView.frame.maxY + inset, width: self.frame.width -  2 * inset, height: textHeight)
            
        }
    }
}
