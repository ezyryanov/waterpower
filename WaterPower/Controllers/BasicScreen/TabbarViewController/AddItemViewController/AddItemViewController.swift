//
//  AddItemViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 27/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit
import CoreData

class AddItemViewController: BasicViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    let inset : CGFloat = 10.0
    let barHeight : CGFloat = 30.0
//    let leftButton = UIButton()
//    let rightButton = UIButton()
//    let categoryText = UILabel()
    let searchBar = UISearchBar(frame: .zero)
//    let searchTextField = UITextField()
//    let searchButton = UIButton()
    let searchSeparator = UIView()
    var itemsCollectionView : UICollectionView! = nil
    var chooseVolumeScreen : ChooseVolumeScreen = ChooseVolumeScreen()
    
    var isDrinkController = true
    var categoryArray : Array<Any>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createElements()
        placeElements()
        // Do any additional setup after loading the view.
        
        if (isDrinkController) {
            categoryArray = CurrentConfig.drinkCategories
//            for drinkCategory in categoryArray {
//                if (drinkCategory as! DrinkCategory).isSelected {
//                    categoryText.text = (drinkCategory as! DrinkCategory).name
//                }
//            }
        } else {
            categoryArray = CurrentConfig.foodCategories
//            for foodCategory in categoryArray {
//                if (foodCategory as! FoodCategory).isSelected {
//                    categoryText.text = (foodCategory as! FoodCategory).name
//                }
//            }
        }
        
        navigationController?.navigationBar.items?.first?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func createElements() {
        
//        let leftButtonImage = UIImage.localImage("left-arrow", template: true)
//        leftButton.setBackgroundImage(leftButtonImage , for: UIControl.State.normal)
//        leftButton.tintColor = Colors.mainColor
//        leftButton.addTarget(self, action: #selector(previousCategoryClick), for: .touchDown)
//
//        let rightButtonImage = UIImage.localImage("right-arrow", template: true)
//        rightButton.setBackgroundImage(rightButtonImage, for: UIControl.State.normal)
//        rightButton.tintColor = Colors.mainColor
//        rightButton.addTarget(self, action: #selector(nextCategoryClick), for: .touchDown)
//
//        categoryText.textAlignment = .center
//        categoryText.textColor = Colors.mainColor
//        categoryText.font = categoryText.font.withSize(20)
        
        searchSeparator.backgroundColor = Colors.mainColor
        
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        
//        let searchButtonImage = UIImage.localImage("search", template: true)
//        searchButton.setBackgroundImage(searchButtonImage , for: UIControl.State.normal)
//        searchButton.tintColor = Colors.mainColor
//        searchButton.addTarget(self, action: #selector(searchButtonClicked), for: .touchDown)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: 100, height: 100)
        
        
        itemsCollectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        itemsCollectionView.dataSource = self
        itemsCollectionView.delegate = self
        itemsCollectionView.register(AddItemCollectionViewCell.self, forCellWithReuseIdentifier: "AddItemCell")
        itemsCollectionView.isUserInteractionEnabled = true
        itemsCollectionView.backgroundColor = UIColor.clear
        self.view.addSubview(itemsCollectionView)
        
//        self.view.addSubview(leftButton)
//        self.view.addSubview(rightButton)
//        self.view.addSubview(categoryText)
        self.view.addSubview(searchBar)
//        self.view.addSubview(searchButton)
//        self.view.addSubview(searchTextField)
        self.view.addSubview(searchSeparator)
        //self.view.addSubview(itemsCollectionView)
    }
    
    func placeElements() {
        var currentPosition = inset
        searchBar.frame = CGRect(x: inset, y: currentPosition, width: self.view.frame.width - 2 * inset, height: barHeight)
//        searchTextField.layer.cornerRadius = searchTextField.frame.height/2.0
//        searchTextField.layer.borderColor = Colors.mainColor.cgColor
//        searchTextField.layer.borderWidth = 2.0
        
//        searchButton.frame = CGRect(x: searchTextField.frame.maxX + inset, y: currentPosition, width: barHeight, height: barHeight)
        currentPosition += searchBar.frame.height + inset
        
//        leftButton.frame = CGRect(x: inset, y: currentPosition, width: barHeight, height: barHeight)
//        rightButton.frame = CGRect(x: self.view.frame.width - barHeight - inset, y: currentPosition, width: barHeight, height: barHeight)
//        categoryText.frame = CGRect(x: leftButton.frame.maxX + inset, y: currentPosition, width: rightButton.frame.minX - leftButton.frame.maxX - 2.0 * inset, height: barHeight)
//
//        currentPosition += categoryText.frame.height + inset
        
        let collectionViewFrame = CGRect(x: 0.0, y: currentPosition, width: self.view.frame.width, height: self.view.frame.maxY - self.view.frame.minY - currentPosition - 2 * inset - 44.0)
        itemsCollectionView.frame = collectionViewFrame
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        title = "Add Item"
    }
    
//    @objc func previousCategoryClick () {
//        changeCategory(isForward: false)
//    }
//    
//    @objc func nextCategoryClick () {
//        changeCategory(isForward: true)
//    }
    
    @objc func searchButtonClicked() {
    
    }
    
//    func changeCategory(isForward : Bool) {
//        if (isDrinkController) {
//            for drinkCategory in categoryArray {
//                if (drinkCategory as! DrinkCategory).isSelected {
//                    let index : NSInteger = (categoryArray as! [DrinkCategory]).firstIndex(where: {$0 === (drinkCategory as! DrinkCategory)}) ?? 0
//                    (categoryArray[index] as! DrinkCategory).isSelected = false
//                    if isForward {
//                        categoryText.text = (categoryArray[(categoryArray.count + index + 1) % categoryArray.count] as! DrinkCategory).name
//                        (categoryArray[(categoryArray.count + index + 1) % categoryArray.count] as! DrinkCategory).isSelected = true
//                    } else {
//                        categoryText.text = (categoryArray[(categoryArray.count + index - 1) % categoryArray.count] as! DrinkCategory).name
//                        (categoryArray[(categoryArray.count + index - 1) % categoryArray.count] as! DrinkCategory).isSelected = true
//                    }
//                    break
//                }
//            }
//        } else {
//            for foodCategory in categoryArray {
//                if (foodCategory as! FoodCategory).isSelected {
//                    let index : NSInteger = (categoryArray as! [FoodCategory]).firstIndex(where: {$0 === (foodCategory as! FoodCategory)}) ?? 0
//                    (categoryArray[index] as! FoodCategory).isSelected = false
//                    if isForward {
//                        categoryText.text = (categoryArray[(categoryArray.count + index + 1) % categoryArray.count] as! FoodCategory).name
//                        (categoryArray[(categoryArray.count + index + 1) % categoryArray.count] as! FoodCategory).isSelected = true
//                    } else {
//                        categoryText.text = (categoryArray[(categoryArray.count + index - 1) % categoryArray.count] as! FoodCategory).name
//                        (categoryArray[(categoryArray.count + index - 1) % categoryArray.count] as! FoodCategory).isSelected = true
//                    }
//                    break
//                }
//            }
//        }
//    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (isDrinkController) {
            return CurrentConfig.drinks.count
        } else {
            return CurrentConfig.food.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (isDrinkController) {
            let myCell : AddItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddItemCell", for: indexPath) as! AddItemCollectionViewCell
            myCell.putModel(drink: CurrentConfig.drinks[indexPath.row])
            myCell.backgroundColor = UIColor.clear
            return myCell
        } else {
            let myCell : AddItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddItemCell", for: indexPath) as! AddItemCollectionViewCell
            myCell.putModel(food: CurrentConfig.food[indexPath.row])
            myCell.backgroundColor = UIColor.clear
            return myCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let managedContext = CoreDataManager.shared.persistentContainer.viewContext
//        let entity =  NSEntityDescription.entity(forEntityName: "Analytics", in: managedContext)
//        let analytics = NSManagedObject(entity: entity!, insertInto: managedContext)
//
//        let cell = collectionView.cellForItem(at: indexPath) as! AddItemCollectionViewCell
//        if (isDrinkController) {
//            let drinkModel = DrinkModel.getDrinkById(id: cell.itemId)
//            analytics.setValue(drinkModel.id, forKey: "drinkID")
//            analytics.setValue(200, forKey: "weightID")
//            analytics.setValue(true, forKey: "isDrink")
//            analytics.setValue(Date.init(timeIntervalSinceNow:  0.0), forKey: "date")//-60.0 * 60.0 * 24.0
//        } else {
//            let foodModel = FoodModel.getFoodById(id: cell.itemId)
//            analytics.setValue(foodModel.id, forKey: "drinkID")
//            analytics.setValue(200, forKey: "weightID")
//            analytics.setValue(false, forKey: "isDrink")
//            analytics.setValue(Date.init(timeIntervalSinceNow: 0.0), forKey: "date")
//        }
//
//        CoreDataManager.shared.saveContext()
        chooseVolumeScreen.frame = self.view.bounds
        self.view.addSubview(chooseVolumeScreen)
        
    }
    
    
    

    
}
