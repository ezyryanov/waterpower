//
//  SubscriptionViewController.swift
//  WaterPower
//
//  Created by Егор Зырянов on 11/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

class SubscriptionViewController: UIViewController {
    
    var isYearSubChosen = true
    let upperView = UIView()
    let barHeight : CGFloat = 80.0
    let conditionBarHeight : CGFloat = 56.0
    let actionButtonHeight : CGFloat = 40.0
    let inset : CGFloat = 15.0
    let subscriptionContainerHeight : CGFloat = 100.0
    let closeButton = UIButton()
    let restoreButton = UIButton()
    
    let scrollView = UIScrollView()
    let premiumTitle = UILabel()
    let advantageOneLabel = UILabel()
    let advantageTwoLabel = UILabel()
    let advantageThreeLabel = UILabel()
    
    let monthSubContainerShadow = UIView()
    let monthSubContainer = UIView()
    let monthSubCheckBorder = UIView()
    let monthSubCheckCenter = UIView()
    let monthSubAction = UILabel()
    let monthSubConditions = UILabel()
    
    let yearSubContainerShadow = UIView()
    let yearSubContainer = UIView()
    let yearSubCheckBorder = UIView()
    let yearSubCheckCenter = UIView()
    let yearSubAction = UILabel()
    let yearSubConditions = UILabel()
    let yearSubSeparator = UILabel()
    let yearSubMonthLabel = UILabel()
    let yearSubMonthPrice = UILabel()
    let yearSubDiscount = UILabel()
    
    let conditionsLabel = UILabel()
    
    let actionButton = UIButton()
    
    let termsLabel = UILabel()
    
    let conditionContainer = UIView()
    let termsButton = UIButton()
    let conditionsButton = UIButton()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.view.bounds
        //gradientLayer.colors = [Colors.containerBackgroundColor1.cgColor, Colors.containerBackgroundColor2.cgColor]
//        gradientLayer.startPoint = CGPoint(x: view.center.x, y: barHeight)
//        gradientLayer.endPoint = CGPoint(x: view.center.x, y: view.center.y)
        self.navigationController?.isNavigationBarHidden = true
//        self.view.layer.addSublayer(gradientLayer)
        
        initElements()
        createElements()
        // Do any additional setup after loading the view.
    }
    
    func initElements() {
        view.addSubview(upperView)
        upperView.addSubview(closeButton)
        upperView.addSubview(restoreButton)
        
        view.addSubview(scrollView)
        scrollView.addSubview(premiumTitle)
        scrollView.addSubview(advantageOneLabel)
        scrollView.addSubview(advantageTwoLabel)
        scrollView.addSubview(advantageThreeLabel)
        
        scrollView.addSubview(monthSubContainerShadow)
        monthSubContainerShadow.addSubview(monthSubContainer)
        monthSubContainer.addSubview(monthSubAction)
        monthSubContainer.addSubview(monthSubConditions)
        monthSubContainer.addSubview(monthSubCheckBorder)
        monthSubCheckBorder.addSubview(monthSubCheckCenter)
        
        scrollView.addSubview(yearSubContainerShadow)
        yearSubContainerShadow.addSubview(yearSubContainer)
        yearSubContainer.addSubview(yearSubAction)
        yearSubContainer.addSubview(yearSubConditions)
        yearSubContainer.addSubview(yearSubCheckBorder)
        yearSubContainer.addSubview(yearSubSeparator)
        yearSubContainer.addSubview(yearSubMonthLabel)
        yearSubContainer.addSubview(yearSubMonthPrice)
        yearSubContainer.addSubview(yearSubDiscount)
        yearSubCheckBorder.addSubview(yearSubCheckCenter)
        
        scrollView.addSubview(conditionsLabel)
        scrollView.addSubview(actionButton)
        scrollView.addSubview(termsLabel)
        
        view.addSubview(conditionContainer)
        conditionContainer.addSubview(conditionsButton)
        conditionContainer.addSubview(termsButton)
    }
    
    func createElements() {
        upperView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: barHeight)
        upperView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        
        view.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        
        closeButton.addTarget(self, action: #selector(closeSubscriptionScreen), for: .touchDown)
        closeButton.setTitle("X", for: .normal)
        closeButton.frame = CGRect(x: inset, y: inset, width: barHeight - 2 * inset, height: barHeight - 2 * inset)
        closeButton.isUserInteractionEnabled = true
        closeButton.setTitleColor(Colors.mainColor, for: .normal)
        
        restoreButton.addTarget(self, action: #selector(restoreSubscription), for: .touchDown)
        restoreButton.setTitle("Restore Purchases", for: .normal)
        restoreButton.frame = CGRect(x: view.frame.width/2.0 - inset, y: inset, width: view.frame.width/2.0, height: barHeight - 2 * inset)
        restoreButton.isUserInteractionEnabled = true
        restoreButton.setTitleColor(Colors.mainColor, for: .normal)
        restoreButton.contentHorizontalAlignment = .right
    
        scrollView.frame = CGRect(x: 0.0, y: barHeight, width: view.frame.width, height: view.frame.height - conditionBarHeight - barHeight)
        
        
        var currentPosition : CGFloat = inset/2.0
        
        premiumTitle.text = "ПРЕМИУМ"
        premiumTitle.textColor = Colors.mainColor
        premiumTitle.sizeToFit()
        premiumTitle.frame = CGRect(x: view.center.x - premiumTitle.frame.width/2.0, y: currentPosition, width: premiumTitle.frame.width, height: premiumTitle.frame.height)
        //premiumTitle.textColor = .white
        
        currentPosition = currentPosition + premiumTitle.frame.height + inset/2.0
        
        advantageOneLabel.text = "► Дополнительные напитки"
        advantageOneLabel.textColor = Colors.mainColor
        advantageOneLabel.sizeToFit()
        advantageOneLabel.frame = CGRect(x: view.center.x - advantageOneLabel.frame.width/2.0, y: currentPosition, width: advantageOneLabel.frame.width, height: advantageOneLabel.frame.height)
        //advantageOneLabel.textColor = .white
        
        currentPosition = currentPosition + advantageOneLabel.frame.height + inset/2.0
        
        advantageTwoLabel.text = "► Доступ к добавлению еды"
        advantageTwoLabel.textColor = Colors.mainColor
        advantageTwoLabel.sizeToFit()
        advantageTwoLabel.frame = CGRect(x: view.center.x - advantageOneLabel.frame.width/2.0, y: currentPosition, width: advantageTwoLabel.frame.width, height: advantageTwoLabel.frame.height)
        //advantageTwoLabel.textColor = .white

        
        currentPosition = currentPosition + advantageTwoLabel.frame.height + inset/2.0
        
        advantageThreeLabel.text = "► Расширенная аналитика"
        advantageThreeLabel.textColor = Colors.mainColor
        advantageThreeLabel.sizeToFit()
        advantageThreeLabel.frame = CGRect(x: view.center.x - advantageOneLabel.frame.width/2.0, y: currentPosition, width: advantageThreeLabel.frame.width, height: advantageThreeLabel.frame.height)
        //advantageThreeLabel.textColor = .white

        
        currentPosition = currentPosition + advantageThreeLabel.frame.height + inset
        
        monthSubContainerShadow.frame = CGRect(x: view.frame.width * 0.05, y: currentPosition, width: view.frame.width * 0.9, height: subscriptionContainerHeight)
        monthSubContainerShadow.backgroundColor = .white
        monthSubContainerShadow.layer.masksToBounds = false
        monthSubContainerShadow.layer.cornerRadius = 25.0
        monthSubContainerShadow.layer.shadowColor = UIColor.black.cgColor
        monthSubContainerShadow.layer.shadowPath = UIBezierPath(roundedRect: monthSubContainerShadow.bounds, cornerRadius: monthSubContainerShadow.layer.cornerRadius).cgPath
        monthSubContainerShadow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        monthSubContainerShadow.layer.shadowOpacity = 0.5
        monthSubContainerShadow.layer.shadowRadius = 1.0
        monthSubContainerShadow.layer.borderColor = UIColor.lightGray.cgColor
        
        monthSubContainer.frame = monthSubContainerShadow.bounds
        monthSubContainer.layer.cornerRadius = monthSubContainerShadow.layer.cornerRadius
        
        if (!isYearSubChosen) {
            monthSubContainer.layer.borderWidth = 4.0
            monthSubContainer.layer.borderColor = Colors.mainColor.cgColor
        } else {
            monthSubContainer.layer.borderWidth = 0.0
            monthSubContainer.layer.borderColor = UIColor.clear.cgColor
        }
        monthSubContainer.isUserInteractionEnabled = true
        monthSubContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseMonthSub)))
        
        monthSubCheckBorder.frame = CGRect(x: inset, y: 2.0 * monthSubContainer.frame.height / 5.0, width: monthSubContainer.frame.height / 5.0, height: monthSubContainer.frame.height / 5.0)
        monthSubCheckBorder.layer.borderColor = Colors.mainColor.cgColor
        monthSubCheckBorder.layer.borderWidth = 2.0
        monthSubCheckBorder.layer.cornerRadius = monthSubCheckBorder.frame.height/2.0
        
        monthSubCheckCenter.frame = CGRect(x: monthSubCheckBorder.frame.width * 0.2, y: monthSubCheckBorder.frame.width * 0.2, width: monthSubCheckBorder.frame.width * 0.6, height: monthSubCheckBorder.frame.width * 0.6)
        if (!isYearSubChosen) {
            monthSubCheckCenter.backgroundColor = Colors.mainColor
        } else {
            monthSubCheckCenter.backgroundColor = .clear
        }
        monthSubCheckCenter.layer.cornerRadius = monthSubCheckCenter.frame.height/2.0
        
        
        monthSubAction.text = "Попробовать 7 дней бесплатно"
        monthSubAction.font = UIFont(name: monthSubAction.font.fontName, size: 14)
        monthSubAction.numberOfLines = 0
        monthSubAction.textAlignment = .left
        monthSubAction.lineBreakMode = .byWordWrapping
        monthSubAction.textColor = Colors.mainColor
        let monthSubActionWidth : CGFloat = monthSubContainer.frame.width - monthSubCheckBorder.frame.maxX - inset
        monthSubAction.frame = CGRect(x:monthSubCheckBorder.frame.maxX + inset, y: monthSubCheckBorder.frame.height, width: monthSubActionWidth, height: (monthSubAction.text?.height(withConstrainedWidth: monthSubActionWidth, font: monthSubAction.font))!)
        
        monthSubConditions.text = "Далее 459,00 Р / месяц"
        monthSubConditions.font = UIFont(name: monthSubAction.font.fontName, size: 15)
        monthSubConditions.numberOfLines = 0
        monthSubConditions.textAlignment = .left
        monthSubConditions.lineBreakMode = .byWordWrapping
        monthSubConditions.textColor = Colors.mainColor
        let monthSubConditionsWidth : CGFloat = monthSubContainer.frame.width - monthSubCheckBorder.frame.maxX - inset
        monthSubConditions.frame = CGRect(x:monthSubCheckBorder.frame.maxX + inset, y: monthSubAction.frame.maxY + inset/2.0, width: monthSubConditionsWidth, height: (monthSubConditions.text?.height(withConstrainedWidth: monthSubConditionsWidth, font: monthSubConditions.font))!)
    
        currentPosition = currentPosition + monthSubContainerShadow.frame.height + inset/2.0
        
        yearSubContainerShadow.frame = CGRect(x: view.frame.width * 0.05, y: currentPosition, width: view.frame.width * 0.9, height: subscriptionContainerHeight)
        yearSubContainerShadow.backgroundColor = .white
        yearSubContainerShadow.layer.masksToBounds = false
        yearSubContainerShadow.layer.cornerRadius = 25.0
        yearSubContainerShadow.layer.shadowColor = UIColor.black.cgColor
        yearSubContainerShadow.layer.shadowPath = UIBezierPath(roundedRect: yearSubContainerShadow.bounds, cornerRadius: yearSubContainerShadow.layer.cornerRadius).cgPath
        yearSubContainerShadow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        yearSubContainerShadow.layer.shadowOpacity = 0.5
        yearSubContainerShadow.layer.shadowRadius = 1.0
        yearSubContainerShadow.layer.borderColor = UIColor.lightGray.cgColor
        
        yearSubContainer.frame = yearSubContainerShadow.bounds
        yearSubContainer.layer.cornerRadius = yearSubContainerShadow.layer.cornerRadius
        
        if (isYearSubChosen) {
            yearSubContainer.layer.borderWidth = 4.0
            yearSubContainer.layer.borderColor = Colors.mainColor.cgColor
        } else {
            yearSubContainer.layer.borderWidth = 0.0
            yearSubContainer.layer.borderColor = UIColor.clear.cgColor
        }
        yearSubContainer.isUserInteractionEnabled = true
        yearSubContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseYearSub)))
        
        yearSubCheckBorder.frame = CGRect(x: inset, y: 2.0 * yearSubContainer.frame.height / 5.0, width: yearSubContainer.frame.height / 5.0, height: monthSubContainer.frame.height / 5.0)
        yearSubCheckBorder.layer.borderColor = Colors.mainColor.cgColor
        yearSubCheckBorder.layer.borderWidth = 2.0
        yearSubCheckBorder.layer.cornerRadius = yearSubCheckBorder.frame.height/2.0
        
        
        yearSubCheckCenter.frame = CGRect(x: yearSubCheckBorder.frame.width * 0.2, y: yearSubCheckBorder.frame.width * 0.2, width: yearSubCheckBorder.frame.width * 0.6, height: yearSubCheckBorder.frame.width * 0.6)
        yearSubCheckCenter.layer.cornerRadius = yearSubCheckCenter.frame.height/2.0
        if (isYearSubChosen) {
            yearSubCheckCenter.backgroundColor = Colors.mainColor
        } else {
            yearSubCheckCenter.backgroundColor = UIColor.clear
        }
        
        yearSubAction.text = "Попробовать 7 дней бесплатно"
        yearSubAction.font = UIFont(name: yearSubAction.font.fontName, size: 14)
        yearSubAction.numberOfLines = 0
        yearSubAction.textAlignment = .left
        yearSubAction.lineBreakMode = .byWordWrapping
        yearSubAction.textColor = Colors.mainColor
        let yearSubActionWidth : CGFloat = (yearSubContainer.frame.width - yearSubCheckBorder.frame.maxX - inset)   * 0.7
        yearSubAction.frame = CGRect(x:yearSubCheckBorder.frame.maxX + inset, y: yearSubCheckBorder.frame.height, width: yearSubActionWidth, height: (yearSubAction.text?.height(withConstrainedWidth: yearSubActionWidth, font: yearSubAction.font))!)
        
        yearSubConditions.text = "Далее 2690,00 Р / год"
        yearSubConditions.font = UIFont(name: yearSubAction.font.fontName, size: 15)
        yearSubConditions.numberOfLines = 0
        yearSubConditions.textAlignment = .left
        yearSubConditions.lineBreakMode = .byWordWrapping
        yearSubConditions.textColor = Colors.mainColor
        let yearSubConditionsWidth : CGFloat = (yearSubContainer.frame.width - yearSubCheckBorder.frame.maxX - inset)  * 0.7
        yearSubConditions.frame = CGRect(x:yearSubCheckBorder.frame.maxX + inset, y: yearSubAction.frame.maxY + inset/2.0, width: yearSubConditionsWidth, height: (yearSubConditions.text?.height(withConstrainedWidth: yearSubConditionsWidth, font: yearSubConditions.font))!)
        
        yearSubSeparator.frame = CGRect(x: yearSubConditions.frame.maxX, y: 0, width: 2, height: yearSubContainer.frame.height)
        yearSubSeparator.backgroundColor = UIColor.lightGray
        
        yearSubMonthLabel.text = "Стоимость в месяц"
        yearSubMonthLabel.font = UIFont(name: yearSubMonthLabel.font.fontName, size: 10)
        yearSubMonthLabel.numberOfLines = 0
        yearSubMonthLabel.textAlignment = .center
        yearSubMonthLabel.lineBreakMode = .byWordWrapping
        yearSubMonthLabel.textColor = Colors.mainColor
        let yearSubMonthLabelWidth : CGFloat = yearSubContainer.frame.width - yearSubSeparator.frame.maxX - inset
        yearSubMonthLabel.frame = CGRect(x: yearSubSeparator.frame.maxX + inset/2.0, y: inset, width: yearSubMonthLabelWidth, height: (yearSubMonthLabel.text?.height(withConstrainedWidth: yearSubMonthLabelWidth, font: yearSubMonthLabel.font))!)
        
        
        yearSubMonthPrice.text = "339,00 Р"
        yearSubMonthPrice.font = UIFont(name: yearSubAction.font.fontName, size: 10)
        yearSubMonthPrice.numberOfLines = 0
        yearSubMonthPrice.textAlignment = .center
        yearSubMonthPrice.lineBreakMode = .byWordWrapping
        yearSubMonthPrice.textColor = Colors.mainColor
        let yearSubMonthPriceWidth : CGFloat = yearSubContainer.frame.width - yearSubSeparator.frame.maxX - inset
        yearSubMonthPrice.frame = CGRect(x: yearSubSeparator.frame.maxX + inset/2.0, y: yearSubMonthLabel.frame.maxY + 5.0, width: yearSubMonthPriceWidth, height: (yearSubMonthPrice.text?.height(withConstrainedWidth: yearSubMonthPriceWidth, font: yearSubMonthPrice.font))!)
        
        
        yearSubDiscount.text = "-51%"
        yearSubDiscount.font = UIFont(name: yearSubDiscount.font.fontName, size: 22)
        yearSubDiscount.numberOfLines = 0
        yearSubDiscount.textAlignment = .center
        yearSubDiscount.lineBreakMode = .byWordWrapping
        yearSubDiscount.textColor = UIColor.red
        let yearSubDiscountWidth : CGFloat = yearSubContainer.frame.width - yearSubSeparator.frame.maxX - inset
        yearSubDiscount.frame = CGRect(x: yearSubSeparator.frame.maxX + inset/2.0, y: yearSubMonthPrice.frame.maxY + 5.0, width: yearSubDiscountWidth, height: (yearSubDiscount.text?.height(withConstrainedWidth: yearSubDiscountWidth, font: yearSubDiscount.font))!)
        
    
        currentPosition = currentPosition + yearSubContainerShadow.frame.height + inset/2.0
        
        
        conditionsLabel.text = "С вас будет автоматически списано 2690,00 Р после окончания 7 дневного пробного периода. Вы можете удалить подписку в любое время. Если вы выбрали ежемесяную подписку, плата будет взыматься ежемесячно. Если вы выбрали годовую подписку, то плата будет взыматься ежегодно"
        conditionsLabel.font = UIFont(name: conditionsLabel.font.fontName, size: 9)
        conditionsLabel.numberOfLines = 0
        conditionsLabel.textAlignment = .center
        conditionsLabel.lineBreakMode = .byWordWrapping
        conditionsLabel.textColor = .lightGray
        conditionsLabel.frame = CGRect(x: inset, y: currentPosition, width: scrollView.frame.width - 2.0 * inset, height: (conditionsLabel.text?.height(withConstrainedWidth: scrollView.frame.width - 2.0 * inset, font: conditionsLabel.font))!)
        
        
        
        currentPosition = currentPosition + conditionsLabel.frame.height + inset/2.0
        
        actionButton.frame = CGRect(x: 2.0 * inset, y: currentPosition, width: scrollView.frame.width - 4.0 * inset, height: actionButtonHeight)
        actionButton.layer.cornerRadius = actionButtonHeight/2.0
        actionButton.backgroundColor = Colors.mainColor
        actionButton.setTitle("Попробовать бесплатно", for: .normal)
        actionButton.setTitleColor(.white, for: .normal)
        actionButton.layer.borderColor = UIColor.white.cgColor
        actionButton.layer.borderWidth = 1.0
        
        currentPosition = currentPosition + actionButton.frame.height + inset/2.0
        
        termsLabel.text = "С вас будет автоматически списано 2690,00 Р после окончания 7 дневного пробного периода. Вы можете удалить подписку в любое время. Если вы выбрали ежемесяную подписку, плата будет взыматься ежемесячно. Если вы выбрали годовую подписку, то плата будет взыматься ежегодно С вас будет автоматически списано 2690,00 Р после окончания 7 дневного пробного периода. Вы можете удалить подписку в любое время. Если вы выбрали ежемесяную подписку, плата будет взыматься ежемесячно. Если вы выбрали годовую подписку, то плата будет взыматься ежегодно С вас будет автоматически списано 2690,00 Р после окончания 7 дневного пробного периода. Вы можете удалить подписку в любое время. Если вы выбрали ежемесяную подписку, плата будет взыматься ежемесячно. Если вы выбрали годовую подписку, то плата будет взыматься ежегодно"
        termsLabel.font = UIFont(name: conditionsLabel.font.fontName, size: 10)
        termsLabel.numberOfLines = 0
        termsLabel.textAlignment = .center
        termsLabel.lineBreakMode = .byWordWrapping
        termsLabel.textColor = .lightGray
        termsLabel.frame = CGRect(x: inset, y: currentPosition, width: scrollView.frame.width - 2.0 * inset, height: (termsLabel.text?.height(withConstrainedWidth: scrollView.frame.width - 2.0 * inset, font: termsLabel.font))!)
        
        currentPosition = currentPosition + termsLabel.frame.height + inset/2.0
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: currentPosition)
        scrollView.isUserInteractionEnabled = true
        scrollView.isScrollEnabled = true
        
        conditionContainer.frame = CGRect(x: 0.0, y: view.frame.height - conditionBarHeight, width: view.frame.width, height: conditionBarHeight)
        
        
        conditionsButton.addTarget(self, action: #selector(conditionsButtonClicked), for: .touchDown)
        conditionsButton.setTitle("Conditions", for: .normal)
        conditionsButton.sizeToFit()
        conditionsButton.frame = CGRect(x: inset, y: inset, width: conditionsButton.frame.width, height: conditionsButton.frame.height)
        conditionsButton.isUserInteractionEnabled = true
        conditionsButton.setTitleColor(Colors.mainColor, for: .normal)

        
        termsButton.addTarget(self, action: #selector(termsButtonClicked), for: .touchDown)
        termsButton.setTitle("Terms", for: .normal)
        termsButton.sizeToFit()
        termsButton.frame = CGRect(x: view.frame.width - termsButton.frame.width - inset, y: inset, width: termsButton.frame.width, height: termsButton.frame.height)
        termsButton.isUserInteractionEnabled = true
        termsButton.setTitleColor(Colors.mainColor, for: .normal)
    }
    
    @objc func chooseMonthSub() {
        isYearSubChosen = false
        createElements()
    }
    
    @objc func chooseYearSub() {
        isYearSubChosen = true
        createElements()
    }
    
    @objc func closeSubscriptionScreen() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func restoreSubscription() {
        
    }
    
    @objc func termsButtonClicked() {
        
    }
    
    @objc func conditionsButtonClicked() {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
