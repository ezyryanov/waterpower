import UIKit
import CoreData
import UserNotifications

class NotificationManager: NSObject {

    static let shared = NotificationManager()
    
    func createNotifications() {
        let startInfo = interval(forKey: "start_day")
        let endInfo = interval(forKey: "end_day")
        let intervalInfo = interval(forKey: "interval")
        
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()

        
        let startHours = NSInteger(startInfo.hours!)
        let startMinutes = NSInteger(startInfo.minutes!)
        let endHours = NSInteger(endInfo.hours!)
        let endMinutes = NSInteger(endInfo.minutes!)
        let intervalHours = NSInteger(intervalInfo.hours!)
        let intervalMinutes = NSInteger(intervalInfo.minutes!)
        
        
        var sheduleArray = [NSInteger]()
        var interval = intervalHours! * 60 + intervalMinutes!
        var start = startHours! * 60 + startMinutes!
        var end = endHours! * 60 + endMinutes!
        while true {
            sheduleArray.append(start)
            start += interval
            if (start > end) {
                break
            }
        }
        
        func removeAllNotifications() {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
        
        for sheduleTime in sheduleArray {
            var date = DateComponents()
            date.hour = NSInteger(CGFloat(sheduleTime) / 60.0)
            date.minute = sheduleTime % 60
            let calendarTrigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: true)
            
            
            let content = UNMutableNotificationContent()
            content.title = "WaterPower"
            content.body = "It's time to drink water"
            content.badge = 1
            content.sound = UNNotificationSound.default
            
            let request = UNNotificationRequest(identifier: "LocalNotification" + String(sheduleTime), content: content, trigger: calendarTrigger)
            print("LocalNotification" + String(sheduleTime))
            UNUserNotificationCenter.current().add(request) { error in
                if let error = error {
                    print("error")
                    // Do something with error
                } else {
                    // Request was added successfully
                    print("success")
                    UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: {array in
                        print(array.count)
                    })
                }
            }
        }
    }
    
    func interval(forKey: String) -> Periods {
        var periods = [Periods]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Periods")
        
        do {
            periods = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest) as! [Periods]
        } catch {
            print(error.localizedDescription)
        }
        
        for period in periods {
            let periodItem = period as Periods
            if (periodItem.identifier == forKey) {
                return period
            }
        }
        return Periods()
    }
    
}
