//
//  AppRouter.swift
//  WaterPower
//
//  Created by Егор Зырянов on 10/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit


class AppRouter: NSObject {
    var navigationViewController : NavigationViewController!
    var tabbarViewController : TabbarViewController = TabbarViewController()
    var tutorialFirstViewController : FirstPageTourViewController = FirstPageTourViewController()
    var subscriptionViewController : SubscriptionViewController = SubscriptionViewController()
    static let shared = AppRouter()
    
    func initiateRouter(window : UIWindow?) {
        window?.makeKeyAndVisible()
        navigationViewController = NavigationViewController(rootViewController: tabbarViewController)
        navigationViewController.navigationBar.isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : Colors.mainColor]
        navigationViewController.navigationBar.barTintColor = .white
        window?.rootViewController = navigationViewController
    }
    
    func goToMain(window : UIWindow?) {
        window?.rootViewController?.navigationController?.popToRootViewController(animated: true)
    }
    
    func goToTutorial(window : UIWindow?) {
        //window?.rootViewController?.show(tutorialFirstViewController, sender: window?.rootViewController)
        window?.rootViewController?.show(tutorialFirstViewController, sender: nil)
    }
    
    func goToSubscription(window : UIWindow?) {
        //window?.rootViewController?.show(tutorialFirstViewController, sender: window?.rootViewController)
        window?.rootViewController?.show(subscriptionViewController, sender: nil)
    }
}
