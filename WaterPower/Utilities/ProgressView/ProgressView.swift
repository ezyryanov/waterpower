//
//  ProgressView.swift
//  WaterPower
//
//  Created by Егор Зырянов on 14/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class ProgressView: UIView {

    static var maxLimit : CGFloat = 2640.0
    static var progressValue : CGFloat = 1230.0
    let progressBarContainer = UIView()
    let progressBarIndicator = UIView()
    let progressText = UILabel()
    let inset : CGFloat = 10.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(progressBarContainer)
        progressBarContainer.addSubview(progressBarIndicator)
        progressBarContainer.addSubview(progressText)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            progressBarContainer.frame = CGRect(x: inset, y: inset, width: self.frame.width - 2.0 * inset, height: self.frame.height - 2.0 * inset)
            progressBarContainer.layer.cornerRadius = progressBarContainer.frame.height/2.0
            progressBarContainer.layer.masksToBounds = true
            progressBarContainer.backgroundColor = Colors.mainColor
            
            progressBarIndicator.frame = CGRect(x: 0.0, y: 0.0, width: progressBarContainer.frame.width * ProgressView.progressValue / ProgressView.maxLimit, height: progressBarContainer.frame.height)
            progressBarIndicator.backgroundColor = Colors.progressColor
            progressBarIndicator.layer.cornerRadius = progressBarIndicator.frame.height/4.0
            
            
        }
    }

}
