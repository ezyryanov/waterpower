//
//  AddItemButton.swift
//  WaterPower
//
//  Created by Егор Зырянов on 14/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class AddItemButton: UIView {

    let button : UIButton = UIButton()
    let inset : CGFloat = 5.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        button.backgroundColor = Colors.mainColor
        self.addSubview(button)
    }
    
    func addTargetToButton(target : NSObject,action : Selector) {
        self.button.addTarget(target, action: action, for: UIControl.Event.touchDown)
    }
    
    override var frame: CGRect {
        didSet {
            button.frame = CGRect(x: inset, y: inset, width: frame.width - 2 * inset, height: frame.height - 2 * inset)
            button.layer.cornerRadius = (frame.height - 2 * inset)/2.0
            button.layer.masksToBounds = true
        }
    }
    
    func setTitle(titleString : String) {
        self.button.setTitle(titleString, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
