//
//  ItemsTableView.swift
//  WaterPower
//
//  Created by Егор Зырянов on 14/05/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class ItemsTableView: UIView, UITableViewDelegate, UITableViewDataSource {
    var addedDrinksArray = [Analytics]()
    var filteredAddedDrinkArray = [Analytics]()
    var currentDate : Date = Date(timeIntervalSinceNow: 0.0)
    let tableView = UITableView()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredAddedDrinkArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddedItemTableViewCell(style: .default, reuseIdentifier: "defaultCell", item: filteredAddedDrinkArray[indexPath.row])
        //cell.textLabel?.text = String(addedDrinksArray[indexPath.row].drinkID)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.height/4.0
    }
    
    func setItemsArray(itemsArray : [Analytics]) {
        addedDrinksArray = itemsArray
        filterArrayByDate()
    }
    
    func filterArrayByDate () {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        filteredAddedDrinkArray.removeAll()
        for analitycItem in addedDrinksArray {
            if Calendar.current.compare(currentDate, to: analitycItem.date! as Date, toGranularity: .day) == .orderedSame {
                filteredAddedDrinkArray.append(analitycItem)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override var frame: CGRect {
        didSet {
            tableView.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: self.frame.height)
        }
    }

}
