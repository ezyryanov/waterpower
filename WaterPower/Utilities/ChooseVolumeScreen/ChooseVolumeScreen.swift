import UIKit

class ChooseVolumeScreen: UIView {

    var itemId : NSInteger = 0
    var isDrink : Bool = true
    var tableView : UITableView = UITableView()
    var switcherView : SwitcherView = SwitcherView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 0.3)
        
        tableView.separatorColor = Colors.mainColor
        tableView.backgroundColor = Colors.mainColor
        self.addSubview(tableView)
        
        switcherView.backgroundColor = Colors.mainColor
        self.addSubview(switcherView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            switcherView.frame = CGRect(x: (self.frame.width - SwitcherView.width())/2.0, y: self.frame.height * 0.2, width: SwitcherView.width(), height: SwitcherView.height())
            
            tableView.frame = CGRect(x: self.frame.width * 0.1, y: self.frame.height * 0.2 + SwitcherView.height() + 10.0, width: self.frame.width * 0.8, height: self.frame.height * 0.6 - SwitcherView.height() - 10.0)
        }
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
    }
}
