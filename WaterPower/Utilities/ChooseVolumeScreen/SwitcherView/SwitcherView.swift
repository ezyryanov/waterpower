//
//  SwitcherView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/08/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class SwitcherView: UIView {

    static func height () -> CGFloat {
        return 56.0
    }
    
    static func width () -> CGFloat {
        return 200.0
    }

}
