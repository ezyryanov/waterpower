import UIKit

class InitialSplashScreen: UIView {

    let closeButton = UIButton()
    let inset : CGFloat = 10.0
    let buttonSize : CGFloat = 40.0
    let genderChoiceView = GenderChoiceView()
    let weightChoiceView = WeightChoiceView()
    let activityCheckView = ActivityCheckView()
    let notificationCheckView = NotificationCheckView()
    let continueButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.addSubview(genderChoiceView)
        self.addSubview(weightChoiceView)
        self.addSubview(activityCheckView)
        self.addSubview(notificationCheckView)
        self.addSubview(continueButton)
        
        continueButton.layer.cornerRadius = buttonSize/2.0
        continueButton.backgroundColor = Colors.mainColor
        continueButton.addTarget(self, action: #selector(closeView), for: .touchDown)
        continueButton.setTitle("Continue", for: .normal)
        continueButton.setTitleColor(.white, for: .normal)
    }
    
    @objc func closeView() {
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            genderChoiceView.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: genderChoiceView.height())
            
            weightChoiceView.frame = CGRect(x: 0.0, y: genderChoiceView.frame.maxY, width: self.frame.width, height: weightChoiceView.height())
            
            activityCheckView.frame = CGRect(x: 0.0, y: weightChoiceView.frame.maxY, width: self.frame.width, height: activityCheckView.height())
            
            notificationCheckView.frame = CGRect(x: 0.0, y: activityCheckView.frame.maxY, width: self.frame.width, height: notificationCheckView.height())
            
            continueButton.frame = CGRect(x: inset, y: notificationCheckView.frame.maxY, width: self.frame.width - 2.0 * inset, height: buttonSize)
        }
    }
    
}
