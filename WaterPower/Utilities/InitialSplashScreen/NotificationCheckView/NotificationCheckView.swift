//
//  NotificationCheckView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/07/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class NotificationCheckView: UIView,UITableViewDataSource,UITableViewDelegate,AlarrmTurnOnTableViewCellDelegate {
    
    let alarmTable = UITableView()
    let cellHeight : CGFloat = 48.0

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpTable()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpTable() {
        alarmTable.frame = frame
        self.addSubview(alarmTable)
        alarmTable.backgroundColor = .clear
        alarmTable.dataSource = self
        alarmTable.delegate = self
        alarmTable.separatorColor = .clear
        alarmTable.bounces = false
        alarmTable.bouncesZoom = false
        alarmTable.tableFooterView = UIView()
        alarmTable.separatorInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1;
        case 1:
            return 3;
        default:
            return 0;
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0 && indexPath.row == 0) {
            let cell = AlarrmTurnOnTableViewCell(style: .default, reuseIdentifier: "turnon")
            cell.tableView = tableView
            cell.delegate = self
            return cell
        } else {
            let cell : AlarmPeriodTableViewCell!
            switch indexPath.row {
            case 0:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Начало дня",pickerType: "start_day")
                break
            case 1:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Конец дня",pickerType:"end_day")
                break
            case 2:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Интервал",pickerType:"interval")
                break
            default:
                cell = AlarmPeriodTableViewCell(style: .default, reuseIdentifier: "period",text: "Неизвестно",pickerType:"interval")
                break
            }
            return cell
        }
    }
    
    func enableNotification() {
        alarmTable.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    override var frame: CGRect {
        didSet {
            alarmTable.frame = self.bounds
        }
    }
    
    func height() -> CGFloat {
        return cellHeight * 4.0
    }

}
