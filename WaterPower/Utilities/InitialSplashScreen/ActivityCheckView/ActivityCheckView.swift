//
//  ActivityCheckView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/07/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class ActivityCheckView: UIView {

    let activityTitleLabel = UILabel()
    let switchView = UISwitch()
    let inset : CGFloat = 10.0
    let labelHeight : CGFloat = 25.0
    let weightLabelWidth : CGFloat = 50.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        activityTitleLabel.textAlignment = .left
        activityTitleLabel.textColor = Colors.mainColor
        activityTitleLabel.text = "Are you active?"
        
        self.addSubview(switchView)
        self.addSubview(activityTitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            switchView.frame = CGRect(x: self.frame.width - inset - switchView.frame.width, y: inset, width: switchView.frame.width, height: switchView.frame.height)
            
            activityTitleLabel.frame = CGRect(x: inset, y: inset, width: self.frame.width - 3.0 * inset - switchView.frame.width, height: labelHeight)
        }
    }
    
    func height() -> CGFloat {
        return (labelHeight + 2.0 * inset)
    }

}
