//
//  GenderChoiceView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/07/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class GenderChoiceView: UIView {

    let switchView = UISwitch()
    let titleLabel = UILabel()
    let maleLabel = UILabel()
    let femaleLabel = UILabel()
    let inset : CGFloat = 10.0
    let labelHeight : CGFloat = 25.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.textAlignment = .center
        titleLabel.textColor = Colors.mainColor
        titleLabel.text = "Gender"
        
        maleLabel.textAlignment = .right
        maleLabel.textColor = Colors.mainColor
        maleLabel.text = "Male"
        
        femaleLabel.textAlignment = .left
        femaleLabel.textColor = Colors.mainColor
        femaleLabel.text = "Female"
        
        self.addSubview(switchView)
        self.addSubview(titleLabel)
        self.addSubview(maleLabel)
        self.addSubview(femaleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            titleLabel.frame = CGRect(x: inset, y: inset, width: self.frame.width - 2.0 * inset, height: labelHeight)
            
            switchView.frame = CGRect(x: self.center.x - switchView.frame.width/2.0, y: titleLabel.frame.maxY + inset, width: switchView.frame.width, height: switchView.frame.height)
            
            femaleLabel.frame = CGRect(x: inset, y: titleLabel.frame.maxY + inset, width: self.frame.width/2.0 - 2.0 * inset - switchView.frame.width/2.0, height: labelHeight)
            
            maleLabel.frame = CGRect(x: switchView.frame.maxX + inset, y: titleLabel.frame.maxY + inset, width: self.frame.width/2.0 - 2.0 * inset - switchView.frame.width/2.0, height: labelHeight)
        }
    }
    
    func height() -> CGFloat {
        return (labelHeight * 2.0 + 3.0 * inset)
    }

}
