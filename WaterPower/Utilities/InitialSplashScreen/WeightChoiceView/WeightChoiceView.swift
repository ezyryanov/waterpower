//
//  WeightChoiceView.swift
//  WaterPower
//
//  Created by Egor Zyryanov on 04/07/2019.
//  Copyright © 2019 Егор Зырянов. All rights reserved.
//

import UIKit

class WeightChoiceView: UIView {

    let titleLabel = UILabel()
    let weightLabel = UILabel()
    let slider = UISlider()
    let inset : CGFloat = 10.0
    let labelHeight : CGFloat = 25.0
    let weightLabelWidth : CGFloat = 50.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        titleLabel.textAlignment = .center
        titleLabel.textColor = Colors.mainColor
        titleLabel.text = "Weight"
        
        weightLabel.textAlignment = .right
        weightLabel.textColor = Colors.mainColor
        weightLabel.text = "87"
        weightLabel.textAlignment = .center
        
        slider.maximumValue = 150.0
        slider.minimumValue = 40.0
        
        self.addSubview(slider)
        self.addSubview(titleLabel)
        self.addSubview(weightLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            titleLabel.frame = CGRect(x: inset, y: inset, width: self.frame.width - 2.0 * inset, height: labelHeight)
            
            
            weightLabel.frame = CGRect(x: self.frame.width - weightLabelWidth - inset, y: titleLabel.frame.maxY + inset, width: weightLabelWidth, height: labelHeight)
            
            slider.frame = CGRect(x: inset, y: titleLabel.frame.maxY + inset, width: self.frame.width - 3.0 * inset - weightLabelWidth, height: labelHeight)
            
        }
    }
    
    func height() -> CGFloat {
        return (labelHeight * 2.0 + 3.0 * inset)
    }

}
