import UIKit

class DateSpinner: UIView {
    
    let leftButton = UIButton()
    let rightButton = UIButton()
    let dateText = UILabel()
    let inset : CGFloat = 10.0
    var currentDate = Date(timeIntervalSinceNow: 0.0)
    var itemsTableView : ItemsTableView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(leftButton)
        self.addSubview(rightButton)
        self.addSubview(dateText)
        
        let leftButtonImage = UIImage.localImage("left-arrow", template: true)
        leftButton.setBackgroundImage(leftButtonImage , for: UIControl.State.normal)
        leftButton.tintColor = Colors.mainColor
        leftButton.addTarget(self, action: #selector(earlierDateClick), for: .touchDown)
        
        let rightButtonImage = UIImage.localImage("right-arrow", template: true)
        rightButton.setBackgroundImage(rightButtonImage, for: UIControl.State.normal)
        rightButton.tintColor = Colors.mainColor
        rightButton.addTarget(self, action: #selector(futureDateClick), for: .touchDown)
        
        dateText.textAlignment = .center
        dateText.textColor = Colors.mainColor
        dateText.font = dateText.font.withSize(20)
    
        
    }
    
    @objc func earlierDateClick() {
        currentDate = Date(timeInterval: -60.0 * 60.0 * 24.0, since: currentDate)
        placeDate()
        itemsTableView.currentDate = currentDate
        itemsTableView.filterArrayByDate()
        itemsTableView.tableView.reloadData()
    }
    
    @objc func futureDateClick() {
        if (Date(timeIntervalSinceNow: 0.0) > Date(timeInterval: +60.0 * 60.0 * 24.0, since: currentDate)) {
            currentDate = Date(timeInterval: +60.0 * 60.0 * 24.0, since: currentDate)
            placeDate()
            itemsTableView.currentDate = currentDate
            itemsTableView.filterArrayByDate()
            itemsTableView.tableView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override var frame: CGRect {
        didSet {
            leftButton.frame = CGRect(x: inset, y: inset, width: self.frame.height - 2.0 * inset, height: self.frame.height - 2.0 * inset)
            rightButton.frame = CGRect(x: self.frame.width - self.frame.height + inset, y: inset, width: self.frame.height - 2.0 * inset, height: self.frame.height - 2.0 * inset)
            dateText.frame = CGRect(x: leftButton.frame.maxX + inset, y: inset, width: rightButton.frame.minX - leftButton.frame.maxX - 2.0 * inset, height: self.frame.height - 2.0 * inset)
            
            placeDate()
        }
    }
    
    func placeDate(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let currentDateString = formatter.string(from: currentDate)
        dateText.text = currentDateString
    }

}
