import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var activityLevel: Bool
    @NSManaged public var gender: Bool
    @NSManaged public var language: String?
    @NSManaged public var waterQuantity: Int32
    @NSManaged public var weight: Int32
    @NSManaged public var metricSystem: Int32

}
