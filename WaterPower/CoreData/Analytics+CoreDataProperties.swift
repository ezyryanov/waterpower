//
//  Analytics+CoreDataProperties.swift
//  
//
//  Created by Egor Zyryanov on 26/06/2019.
//
//

import Foundation
import CoreData


extension Analytics {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Analytics> {
        return NSFetchRequest<Analytics>(entityName: "Analytics")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var drinkID: Int32
    @NSManaged public var isDrink: Bool
    @NSManaged public var weightID: Int32

}
